Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: EClib
Upstream-Contact: John Cremona
Source: https://github.com/JohnCremona/eclib/

Files: *
Copyright: 1990-2022 John Cremona and Marcus Mo
License: GPL-2+
 This software is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This software is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 for more details.
 .
 You should have received a copy of the GNU General Public License
 along with mwrank; if not, write to the Free Software Foundation,
 Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'

Files: libsrc/GetOpt.cc
Copyright: 1987, 1989 Free Software Foundation, Inc.
License: LGPL-2+ with g++ use exception
 (Modified by Douglas C. Schmidt for use with GNU G++.)
 This file is part of the GNU C++ Library.  This library is free
 software; you can redistribute it and/or modify it under the terms of
 the GNU Library General Public License as published by the Free
 Software Foundation; either version 2 of the License, or (at your
 option) any later version.  This library is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU Library General Public License for more details.
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free Software
 Foundation, 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General Public
 License can be found in `/usr/share/common-licenses/LGPL-2'

Files: m4/*
Copyright: 2008 Benjamin Kosnik <bkoz@redhat.com>
	   2008 Daniel Casimiro <dan.casimiro@gmail.com>
	   2008 Michael Tindal
	   2008 Pete Greenwell <pete@mu.org>
	   2008 Thomas Porschberg <thomas@randspringer.de>
	   2009 Michael Tindal
	   2009 Peter Adolphs
	   2009 Thomas Porschberg <thomas@randspringer.de>
	   2012 Zack Weinberg <zackw@panix.com>
	   2013 Roy Stogner <roystgnr@ices.utexas.edu>
License: all-permissive
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.

Files: debian/*
Copyright: 2012-2022 Julien Puydt <jpuydt@debian.org>
License: LGPL-2+
 On Debian systems, the complete text of the GNU Library General Public
 License can be found in `/usr/share/common-licenses/LGPL-2'
